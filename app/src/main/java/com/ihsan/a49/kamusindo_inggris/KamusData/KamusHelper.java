package com.ihsan.a49.kamusindo_inggris.KamusData;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.ihsan.a49.kamusindo_inggris.Database.DatabaseHelper;

import java.util.ArrayList;

import static com.ihsan.a49.kamusindo_inggris.Database.DatabaseContract.KamusColumns.ARTI;
import static com.ihsan.a49.kamusindo_inggris.Database.DatabaseContract.KamusColumns.ID;
import static com.ihsan.a49.kamusindo_inggris.Database.DatabaseContract.KamusColumns.KATA;
import static com.ihsan.a49.kamusindo_inggris.Database.DatabaseContract.TABLE_ENGLISH;
import static com.ihsan.a49.kamusindo_inggris.Database.DatabaseContract.TABLE_INDONESIA;

public class KamusHelper {

    private Context context;
    private DatabaseHelper databaseHelper;
    private String table;
    private SQLiteDatabase database;

    public KamusHelper(Context context) {
        this.context = context;
    }

    public KamusHelper open() throws SQLException {
        databaseHelper = new DatabaseHelper(context);
        database = databaseHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        databaseHelper.close();
    }

    public ArrayList<KamusModel> getDataByKata(String kata, Boolean english) {

        if (english) {
            table = TABLE_ENGLISH;
        } else {
            table = TABLE_INDONESIA;
        }

        Cursor cursor = database.query(table, null, KATA + " LIKE ?",
                new String[]{kata}, null, null, ID + " ASC", null);
        cursor.moveToFirst();
        ArrayList<KamusModel> arrayList = new ArrayList<>();
        KamusModel kamusModel;
        if (cursor.getCount() > 0) {
            do {
                kamusModel = new KamusModel();
                kamusModel.setId(cursor.getInt(cursor.getColumnIndexOrThrow(ID)));
                kamusModel.setKata(cursor.getString(cursor.getColumnIndexOrThrow(KATA)));
                kamusModel.setArti(cursor.getString(cursor.getColumnIndexOrThrow(ARTI)));

                arrayList.add(kamusModel);
                cursor.moveToNext();

            } while (!cursor.isAfterLast());
        }
        cursor.close();
        return arrayList;
    }

    public ArrayList<KamusModel> getAllData(Boolean english) {
        if (english) {
            table = TABLE_ENGLISH;
        } else {
            table = TABLE_INDONESIA;
        }

        Cursor cursor = database.query(table, null, null, null,
                null, null, ID + " ASC", null);
        cursor.moveToFirst();
        ArrayList<KamusModel> arrayList = new ArrayList<>();
        KamusModel kamusModel;
        if (cursor.getCount() > 0) {
            do {
                kamusModel = new KamusModel();
                kamusModel.setId(cursor.getInt(cursor.getColumnIndexOrThrow(ID)));
                kamusModel.setKata(cursor.getString(cursor.getColumnIndexOrThrow(KATA)));
                kamusModel.setArti(cursor.getString(cursor.getColumnIndexOrThrow(ARTI)));

                arrayList.add(kamusModel);
                cursor.moveToNext();

            } while (!cursor.isAfterLast());
        }
        cursor.close();
        return arrayList;

    }

    public long insert(KamusModel kamusModel) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KATA, kamusModel.getKata());
        initialValues.put(ARTI, kamusModel.getArti());
        return database.insert(TABLE_INDONESIA, null, initialValues);
    }

    public void beginTransaction() {
        database.beginTransaction();
    }

    public void setTransactionSuccess() {
        database.setTransactionSuccessful();
    }

    public void endTransaction() {
        database.endTransaction();
    }

    public void insertTransaction(KamusModel kamusModel, Boolean english) {
        if (english) {
            table = TABLE_ENGLISH;
        } else {
            table = TABLE_INDONESIA;
        }
        String sql = "INSERT INTO " + table + " (" +
                KATA + ", " + ARTI + ") VALUES (?, ?)";
        SQLiteStatement stmt = database.compileStatement(sql);
        stmt.bindString(1, kamusModel.getKata());
        stmt.bindString(2, kamusModel.getArti());
        stmt.execute();
        stmt.clearBindings();
    }

    public int update(KamusModel kamusModel) {
        ContentValues args = new ContentValues();
        args.put(KATA, kamusModel.getKata());
        args.put(ARTI, kamusModel.getArti());
        return database.update(TABLE_INDONESIA, args, ID + "= '" + kamusModel.getId() + "'", null);
    }

    public int delete(int id) {
        return database.delete(TABLE_INDONESIA, ID + "= '" + id + "'", null);
    }
}
