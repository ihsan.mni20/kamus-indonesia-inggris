package com.ihsan.a49.kamusindo_inggris.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.ihsan.a49.kamusindo_inggris.Database.DatabaseContract.KamusColumns.ARTI;
import static com.ihsan.a49.kamusindo_inggris.Database.DatabaseContract.KamusColumns.ID;
import static com.ihsan.a49.kamusindo_inggris.Database.DatabaseContract.KamusColumns.KATA;
import static com.ihsan.a49.kamusindo_inggris.Database.DatabaseContract.TABLE_ENGLISH;
import static com.ihsan.a49.kamusindo_inggris.Database.DatabaseContract.TABLE_INDONESIA;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static String DATABASE_NAME = "dbkamus";

    private static final int DATABASE_VERSION = 1;

    private static String CREATE_TABLE_INDONESIA = "create table " + TABLE_INDONESIA + " (" + ID +
            " integer primary key autoincrement, " +
            KATA + " text not null, " +
            ARTI + " text not null);";

    private static String CREATE_TABLE_ENGLISH = "create table " + TABLE_ENGLISH + " (" + ID +
            " integer primary key autoincrement, " +
            KATA + " text not null, " +
            ARTI + " text not null);";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_ENGLISH);
        db.execSQL(CREATE_TABLE_INDONESIA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("Drop TABLE IF EXISTS " + TABLE_ENGLISH);
        db.execSQL("Drop TABLE IF EXISTS " + TABLE_INDONESIA);
        onCreate(db);
    }
}
