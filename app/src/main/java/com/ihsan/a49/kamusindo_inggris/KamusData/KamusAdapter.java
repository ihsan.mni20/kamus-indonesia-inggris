package com.ihsan.a49.kamusindo_inggris.KamusData;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ihsan.a49.kamusindo_inggris.ArtiActivity;
import com.ihsan.a49.kamusindo_inggris.R;

import java.util.ArrayList;
import java.util.List;

public class KamusAdapter extends RecyclerView.Adapter<KamusAdapter.HolderData> {

    private List<KamusModel> mLists;
    private Context context;

    public KamusAdapter(Context context) {
        this.context = context;
    }

    class HolderData extends RecyclerView.ViewHolder {
        TextView kata, arti;

        HolderData(View v) {

            super(v);

            kata = v.findViewById(R.id.txtKata);
            arti = v.findViewById(R.id.txtArti);
        }

    }

    @NonNull
    @Override
    public HolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_list, parent, false);
        return new HolderData(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderData holder, int position) {

        final KamusModel md = mLists.get(position);

        holder.arti.setText(md.getArti());
        holder.kata.setText(md.getKata());
        holder.itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ArtiActivity.class);
                intent.putExtra("mKata", md.getKata());
                intent.putExtra("mArti", md.getArti());
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return mLists.size();
    }

    public void addItem(ArrayList<KamusModel> mLists) {
        this.mLists = mLists;
        notifyDataSetChanged();

    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}