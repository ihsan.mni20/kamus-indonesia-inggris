package com.ihsan.a49.kamusindo_inggris;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.core.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.ihsan.a49.kamusindo_inggris.KamusData.KamusAdapter;
import com.ihsan.a49.kamusindo_inggris.KamusData.KamusHelper;
import com.ihsan.a49.kamusindo_inggris.KamusData.KamusModel;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    @BindView(R.id.searchView)
    MaterialSearchView searchView;
    @BindView(R.id.recycleView)
    RecyclerView recycle_view;

    KamusAdapter kamusAdapter;
    KamusHelper kamusHelper;
    Boolean english;
    MenuItem mSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        kamusHelper = new KamusHelper(this);
        kamusAdapter = new KamusAdapter(this);
        recycle_view.setLayoutManager(new LinearLayoutManager(this));
        recycle_view.setAdapter(kamusAdapter);

        kamusHelper.open();
        Intent se = getIntent();

        english = se.getBooleanExtra("english", false);

        if (english) {
            getSupportActionBar().setSubtitle(R.string.inggris_indonesia);
        } else {
            getSupportActionBar().setSubtitle(R.string.indonesia_inggris);
        }

        ArrayList<KamusModel> kamusModels = kamusHelper.getAllData(english);

        kamusHelper.close();
        kamusAdapter.addItem(kamusModels);

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
                intent.putExtra("search", query);
                intent.putExtra("english", english);
                startActivity(intent);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.navView);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main, menu);
        mSearch = menu.findItem(R.id.actionSearch);
        searchView.setMenuItem(mSearch);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return id == R.id.actionSearch || super.onOptionsItemSelected(item);

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.navIndonesia) {
            restart(false);
        } else if (id == R.id.navInggris) {
            restart(true);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void restart(Boolean english) {

        Intent refresh = new Intent(getApplicationContext(), MainActivity.class);
        refresh.putExtra("english", english);
        startActivity(refresh);
        finish();

    }
}
