package com.ihsan.a49.kamusindo_inggris.Database;

import android.provider.BaseColumns;

public class DatabaseContract {

    public static String TABLE_INDONESIA = "table_indonesia";
    public static String TABLE_ENGLISH = "table_english";

    public static final class KamusColumns implements BaseColumns {

        public static String ID = "id";
        public static String KATA = "kata";
        public static String ARTI = "arti";

    }
}
