package com.ihsan.a49.kamusindo_inggris;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ArtiActivity extends AppCompatActivity {


    @BindView(R.id.tvArti)
    TextView tvArti;
    @BindView(R.id.tvKata)
    TextView tvKata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arti);

        ButterKnife.bind(this);

        Intent intent = getIntent();
        String txtArti = intent.getStringExtra("mArti");
        String txtKata = intent.getStringExtra("mKata");

        tvArti.setText(txtArti);
        tvKata.setText(txtKata);


    }
}
