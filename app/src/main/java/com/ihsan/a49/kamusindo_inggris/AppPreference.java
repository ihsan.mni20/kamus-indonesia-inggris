package com.ihsan.a49.kamusindo_inggris;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

class AppPreference {
    private SharedPreferences prefs;
    private Context context;

    AppPreference(Context context) {
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        this.context = context;
    }

    @SuppressLint("ApplySharedPref")
    public void setFirstRun(Boolean input) {
        SharedPreferences.Editor editor = prefs.edit();
        String key = context.getResources().getString(R.string.app_name);
        editor.putBoolean(key, input);
        editor.commit();

    }

    public Boolean getFirstRun() {
        String key = context.getResources().getString(R.string.app_name);
        return prefs.getBoolean(key, true);
    }
}
