package com.ihsan.a49.kamusindo_inggris;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;


import com.ihsan.a49.kamusindo_inggris.KamusData.KamusHelper;
import com.ihsan.a49.kamusindo_inggris.KamusData.KamusModel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class PreloadActivity extends AppCompatActivity {

    ProgressBar progressBar;
    String table;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preload);

        progressBar = findViewById(R.id.progressBarr);

        new LoadData().execute();
    }

    @SuppressLint("StaticFieldLeak")
    private class LoadData extends AsyncTask<Void, Integer, Void> {
        final String TAG = LoadData.class.getSimpleName();
        KamusHelper kamusHelper;
        AppPreference appPreference;
        double progress;
        double maxProgress = 100;

        @Override
        protected void onPreExecute() {
            kamusHelper = new KamusHelper(PreloadActivity.this);
            appPreference = new AppPreference(PreloadActivity.this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            Boolean firstRun = appPreference.getFirstRun();

            if (firstRun) {
                ArrayList<KamusModel> kamusModelsEng = preLoadRaw(true);
                ArrayList<KamusModel> kamusModelsInd = preLoadRaw(false);
                kamusHelper.open();

                progress = 30;
                publishProgress((int) progress);
                Double progressMaxInsert = 80.0;
                Double progressDiff = (progressMaxInsert - progress) / kamusModelsEng.size() + kamusModelsInd.size();

                kamusHelper.beginTransaction();

                try {

                    for (KamusModel model : kamusModelsInd) {
                        kamusHelper.insertTransaction(model, false);
                        progress += progressDiff;
                        publishProgress((int) progress);
                    }

                    for (KamusModel model : kamusModelsEng) {
                        kamusHelper.insertTransaction(model, true);
                        progress += progressDiff;
                        publishProgress((int) progress);
                    }

                    kamusHelper.setTransactionSuccess();
                } catch (Exception e) {

                    Log.e(TAG, "doInBackground: Exception");
                }

                kamusHelper.endTransaction();

                kamusHelper.close();
                appPreference.setFirstRun(false);
                publishProgress((int) maxProgress);

            } else {
                try {
                    synchronized (this) {
                        this.wait(2000);

                        publishProgress(50);

                        this.wait(2000);
                        publishProgress((int) maxProgress);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            progressBar.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Void result) {
            Intent i = new Intent(PreloadActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        }
    }

    public ArrayList<KamusModel> preLoadRaw(Boolean english) {
        Resources res = getResources();
        InputStream raw_dict;
        if (english) {
            raw_dict = res.openRawResource(R.raw.english_indonesia);
        } else {
            raw_dict = res.openRawResource(R.raw.indonesia_english);
        }

        ArrayList<KamusModel> kamusModels = new ArrayList<>();

        BufferedReader reader;

        try {

            reader = new BufferedReader(new InputStreamReader(raw_dict));

            String line;
            do {
                line = reader.readLine();
                if (line == null) {

                    Log.i("kamus", "line null");
                    break;
                }

                String[] splitstr = line.split("\t");

                //Log.i("kamus", "size :"+splitstr[0]+" pas "+splitstr.length);

                KamusModel kamusModel;

                String kat = splitstr[0];
                String ar = splitstr[1];

                kamusModel = new KamusModel(kat, ar);
                kamusModels.add(kamusModel);
            } while (line != null);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return kamusModels;
    }
}
