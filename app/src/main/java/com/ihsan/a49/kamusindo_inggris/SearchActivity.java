package com.ihsan.a49.kamusindo_inggris;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.ihsan.a49.kamusindo_inggris.KamusData.KamusAdapter;
import com.ihsan.a49.kamusindo_inggris.KamusData.KamusHelper;
import com.ihsan.a49.kamusindo_inggris.KamusData.KamusModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchActivity extends AppCompatActivity {
    @BindView(R.id.recycleSearch)
    RecyclerView mRecycler;

    private static final String ARG_SEARCH = "search";

    KamusAdapter kamusAdapter;
    KamusHelper kamusHelper;
    Boolean english;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        kamusHelper = new KamusHelper(this);
        kamusAdapter = new KamusAdapter(this);
        mRecycler.setLayoutManager(new LinearLayoutManager(this));
        mRecycler.setAdapter(kamusAdapter);

        kamusHelper.open();

        Intent se = getIntent();
        String search = se.getStringExtra(ARG_SEARCH);
        english = se.getBooleanExtra("english", false);


        ArrayList<KamusModel> kamusModels = kamusHelper.getDataByKata(search, english);

        kamusHelper.close();
        kamusAdapter.addItem(kamusModels);

    }
}
